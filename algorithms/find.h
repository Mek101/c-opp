#ifndef FIND_H
#define FIND_H

void* find(struct Itinerator* it, char *(pred)(void*));

#endif