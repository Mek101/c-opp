#include "../containers/itinerator/itinerator.h"


void* find(struct Itinerator* it, char *(pred)(void*))
{
	void* item = NULL;
	do
	{
		void* curr = it__getCurrent(it);
		if(pred(curr))
		{
			item = curr;
			break;
		}
	}
	while(it__tryMove(it));
	it__destroy(it);

	return item;
}