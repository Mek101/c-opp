#include <stdio.h>
#include <stdio.h>
#include "find.h"
#include "../containers/vector/vector.h"


char predicate(void* item)
{
	return (*(int*)item) == 10;
}

int main()
{
	struct Vector* vect = vector__new(void);
	int i;
	for(i = 0; i < 30; i++)
	{
		int* val = malloc(sizeof(int));
		*val = i;
		vector__push(vect, val);
	}

	int* res = find(vector__getItinerator(vect), predicate);
	printf("%d\n", *res);
}
