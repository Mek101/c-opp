#ifndef DEFAULT_H
#define DEFAULT_H

#include <string.h> /* for "memcpy" */

/*#define NULL_SELF_CHECK*/

enum Status
{
	OK = 0,
	MemAllocationError = 1,
	NullInstance = 2,
	RangeError = 3,
	Error = 4
};


#ifdef NULL_SELF_CHECK
#define RETURN_NULL_SELF(x) {if((x) == NULL) return NullInstance;}
#else
#define RETURN_NULL_SELF(x) ( (void)(x) )
#endif

/*
	Simple compile time assertion.
	Example: CT_ASSERT(sizeof foo <= 16, foo_can_not_exceed_16_bytes);
	From: stackoverflow.com/a/19355668
*
#define STATIC_ASSERT(exp, message_identifier)	\
	struct static_assertion {					\
		char message_identifier : 8 + !(exp);	\
	}
*/
/*
	Simple compile time bool.
	From: stackoverflow.com/a/42966734
*/
#define STATIC_BOOL(exp) ( typedef char p __LINE__[ (exp) ? 1 : -1] )


/*
	Word bit sizes.
	ARCH_64 for 64-bit, ARCH_32 for 32-bit
*/

/* gcc */
#if __GNUC__
	#if __x86_64__ || __ppc64__
		#define ARCH_64 1
	#else
		#define ARCH_32 1
	#endif
#endif

/* vcc */
#if _WIN32 || _WIN64
	#if _WIN64
		#define ARCH_64 1
	#elif _WIN32
		#define ARCH_32 1
	#endif

	/* Microsoft can be funny sometimes. */
	#if defined(ARCH_32) && defined(ARCH_64)
		#error Both _WIN32 and _WIN64 are defined. Please check your project configuration.
	#endif
#endif

#endif
