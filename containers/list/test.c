#include <stdio.h>
#include "container.h"
#include "list.h"

void main()
{
	int k = 10;
	long ld = 200000.0;
	Container* cnt = container__alloc(NULL, &k, sizeof(k));

	printf("%d\n", container__itemAs(cnt, int));

	/*container__dump(cnt, sizeof(k));*/

	Container* next = container__alloc(NULL, &ld, sizeof(ld));
	container__setNext(cnt, next);

	/*container__dump(cnt, sizeof(k));*/

	printf("%ld\n", container__itemAs(container__getNext(cnt), long));

	container__free(container__getNext(cnt));
	container__free(cnt);



	struct List* lst = list__alloc(sizeof(int));

}
