#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include "container.h"
#include "../default.h"
#include "../itinerator/itinerator.h"

/*
	#### PROTECTED DECLARATIONS ####
*/
Container* _list__getTailContainer(Container* front);


/*
	#### PUBLIC DECLARATIONS ####
*/
/*
	Linked List struct.
*/
struct List
{
	/* Number of items. */
	size_t length;

	Container* _front;

	uint8_t itemSize;
};


#define list__decl(itemSize) { 0, NULL, (itemSize) }

enum Status list__clear(struct List* _self);


struct List* list__alloc(const uint8_t itemSize);

enum Status list__free(struct List* _self);


void* list__getFront(const struct List* _self);

void* list__getTail(const struct List* _self);


struct Itinerator* list__getItinerator(const struct List* _self);


#endif
