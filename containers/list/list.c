#include <stdlib.h>
#include <stdio.h>
#include <alloca.h>
#include "list.h"
#include "container.h"
#include "../default.h"
#include "../itinerator/itinerator.h"

#define sts(c) ( (struct Sts*)(c->_status) )

/***************************************************************************************************
	#### LIST PROTECTED MEMBERS ####
***************************************************************************************************/
Container* _list__getTailContainer(Container* front)
{
	if(front == NULL)
		return NULL;

	Container* last = front;
	while(1)
	{
		#ifdef DEBUG
		printf("last: %p, next %p\n", last, container__getNext(last));
		#endif
		Container* next = container__getNext(last);

		if(next == NULL)
			return last;
		else
			last = next;
	}
}


/***************************************************************************************************
	#### LIST PUBLIC MEMBERS ####
***************************************************************************************************/
struct List* list__alloc(const uint8_t itemSize)
{
	struct List* _self = malloc(sizeof(struct List));
	if(_self == NULL)
		return NULL;

	_self->length = 0;
	_self->_front = NULL;
	_self->itemSize = itemSize;
	return _self;
}


enum Status list__free(struct List* _self)
{
	RETURN_NULL_SELF(_self);
	list__clear(_self);
	free(_self);
	return OK;
}


enum Status list__clear(struct List* _self)
{
	RETURN_NULL_SELF(_self);
	if(_self->_front == NULL)
		return Error;
	
	Container* last = _self->_front;
	while(last != NULL)
	{
		Container* tmp  = container__getNext(last);
		container__free(last);
		last = tmp;
	}
	_self->length = 0;
	return OK;
}


void* list__getFront(const struct List* _self)
{
	#ifdef NULL_SELF_CHECK
	if(_self == NULL || _self->_front == NULL)
	#else
	if(_self->_front == NULL)
	#endif
		return NULL;
	else
		return container__item(_self->_front);
}


void* list__getTail(const struct List* _self)
{
	#ifdef NULL_SELF_CHECK
	if(_self == NULL)
		return NULL;
	#endif
	Container* c = _list__getTailContainer(_self->_front);

	if(c == NULL)
		return NULL;
	else
		return container__item(c);
}


/***************************************************************************************************
	#### LIST ITINERATOR INTERFACE ####
***************************************************************************************************/
struct Sts
{
	Container* cnt;
	Container* front;
};


static void __list__reset(struct Itinerator* it)
{
	sts(it)->cnt = sts(it)->front;
}


static uint8_t __list__tryMove(struct Itinerator* it)
{
	Container* next = container__getNext(sts(it)->cnt);
	if(next == NULL)
		return 0;
	else
	{
		sts(it)->cnt = next;
		return 1;
	}
}


static void* __list__getCurrent(const struct Itinerator* it)
{
	return container__item(sts(it)->cnt);
}


struct Itinerator* list__getItinerator(const struct List* _self)
{
	struct Sts* sts = malloc(sizeof(struct Sts));
	sts->front = _self->_front;

	return it__getItinerator(sts, __list__reset, __list__tryMove, __list__getCurrent, (void (*)(struct Itinerator*))free);
}
