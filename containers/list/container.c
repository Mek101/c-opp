#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "container.h"
#include "../default.h"

/***************************************************************************************************
	#### PUBLIC DECLARATIONS ####
***************************************************************************************************/
Container* container__alloc(Container* next, const void* item, const uint8_t itemSize)
{
	Container* _self = malloc(sizeof(Container*) + itemSize);
	if(_self == NULL)
		return NULL;
	container__setNext(_self, next);
	memcpy(container__item(_self), item, itemSize);
	return _self;
}


#ifdef DEBUG
void container__dump(const Container* _self, const uint8_t itemSize)
{
	size_t i;
	for(i = 0; i < sizeof(Container*) + itemSize; i++)
		printf("container at %d°: %d\n", i, *((Container*)_self + i));
	printf("\n");
}
#endif
