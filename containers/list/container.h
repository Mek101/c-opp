#include <stddef.h>
#include <stdint.h>

#ifndef CONTAINER_H
#define CONTAINER_H


typedef struct {
	void* _next;
} Container;


Container* container__alloc(Container* next, const void* item, const uint8_t itemSize);

#ifdef NULL_SELF_CHECK
#define container__free(c) ( c == NULL ? NullInstance : (free(c), OK) )
#else
#define container__free(c) ( free(c), OK )
#endif


#define container__getNext(c) ( (Container*)((c)->_next) )

#ifdef NULL_SELF_CHECK
#define container__setNext(c, n) ( c == NULL ? NullInstance : ((c)->_next = n, OK) )
#else
#define container__setNext(c, n) ( (c)->_next = n, OK )
#endif


#define container__item(c) ( (void*)( ((uint8_t*)c) + sizeof(Container*) ) )

#define container__itemAs(c, t) ( *(t*)container__item(c) )


#ifdef DEBUG
void container__dump(const Container* _self, const uint8_t itemSize);
#else
#define container__dump(s, i) ( )
#endif

#endif
