#ifndef ORDREDMAP_h
#define ORDREDMAP_h
#include <stdlib.h> /* for "size_t" */
#include "../default.h"
#include "../array/array.h"
#include "../vector/vector.h"
#include "../list/container.h"


struct OrdredMap
{
	/* Number of current key-value pairs. */
	size_t length;
	/* The payload size in containers of keys. */
	size_t capacity;

	/* Pointer to the bytes making the memory area. */
	uint8_t* _payload;

	/* Size in bytes of the containers with the keys. */
	uint8_t _keySize;
	/* Size in bytes of the values. */
	uint8_t _valueSize;
};


#define ordredMap__decl(keySize, valueSize) { 0, 0, NULL, (keySize), (valueSize) }

enum Status ordredMap__clear(struct OrdredMap* _self);


struct OrdredMap* ordredMap__alloc(const uint8_t keySize, const uint8_t valueSize);

enum Status ordredMap__free(struct OrdredMap* _self);



#define ordredMap__resize(v) ( ordredMap__resizeTo((v), (v)->length) )

enum Status ordredMap__resizeTo(struct OrdredMap* _self, const size_t size);



enum Status ordredMap__push(struct OrdredMap* _self, const void* key, const void* value);



enum Status ordredMap__removeBy(struct OrdredMap* _self, const void* key);



void* ordredMap__elementBy(struct OrdredMap* _self, const void* key);





#endif
