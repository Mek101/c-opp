#include <stdlib.h>
#include "ordredMap.h"
#include "../default.h"
#include "../array/array.h"
#include "../vector/vector.h"
#include "../list/container.h"


/*
 *  #### PRIVATE MEMBERS ####
 */
#define __itemPtrAt(ptr, itemSize, i) ( (uint8_t*)((ptr) + ((itemSize) + sizeof(Containers) * (i))) )

#define __payloadAt(o, i) ( __itemPtrAt(o->_payload, o->_keySize, i) )


/*
	Gets the next power of 2 to the given value.
*/
static size_t __ordredMap__nextPow(size_t value)
{
	/* https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 Hack. */
	if(value < 2) return 2;

	value--;
	value |= value >> 1;
	value |= value >> 2;
	value |= value >> 4;
	value |= value >> 8;
	value |= value >> 16;
	#ifdef ARCH_64
	value |= value >> 32;
	#endif
	value++;

	/*
		'size_t' is often of a weird size (eg: sometimes is neither 32 or 64 bits: may even be 48 bits or
		weirder), so correcting to the maximum value if it has passed it.
	*/
	if(value > SIZE_MAX)
		return SIZE_MAX;
	else
		return value;
}


/*
	Confronts itemSize bytes from the given pointers.
	Returns 1 if the contents where equal, 0 otherwise.
*/
static unsigned char __ordredMap__keyCompare(const size_t keySize, const void* it1, const void* it2)
{
	size_t i;
	for(i = sizeof(Container); i < keySize + sizeof(Container); i++)
		if(*__itemPtrAt(it1, keySize, i) != *__itemPtrAt(it2, keySize, i))
			return 0;
	return 1;
}


/*
	Ensures the vector payload has at least the given capacity for itemSize elements.
	Espects a non-null vector.
*/
static enum Status __ordredMap__ensurePayloadCapacity(struct OrdredMap* _self, const size_t reqCapacity)
{
	if(_self->capacity < reqCapacity)
	{
		#ifdef DEBUG
		printf(" OrdredMap::Not enough capacity %lu for %lu° items.\n", _self->capacity, reqCapacity);
		#endif

		/* Adding capacity by nearest power of 2 for enough space. Starting with 2 otherwise. */
		return ordredMap__resizeTo(_self, __ordredMap__nextPow(reqCapacity));
	}
	else
		return OK;
}


static enum Status __ordredMap__insertAt(struct OrdredMap* _self, const size_t index, const void* key, const void* value)
{
	/* Checks if there's enough space for the new element sitting at the next length. */
	enum Status sts = __ordredMap__ensurePayloadCapacity(_self, _self->length + 1);
	if(sts != OK)
		return sts;

	if(index == _self->length)
	{
		Container emptyContainer;
		emptyContainer._next
		memcpy(__payloadAt(_self, index), item, );
	}
	else
	{

	}

	_self->length++;

	return OK;
}


/*
	#### PUBLIC MEMBERS ####
*/


enum Status ordredMap__clear(struct OrdredMap* _self)
{
	RETURN_NULL_SELF(_self);
	if(_self->_payload != NULL)
		free(_self->_payload);
	_self->capacity = 0;
	_self->length = 0;
	return OK;
}


struct OrdredMap* ordredMap__alloc(const uint8_t keySize, const uint8_t valueSize)
{
	struct OrdredMap* _self = malloc(sizeof(struct OrdredMap));
	if(_self == NULL)
		return NULL;
	_self->capacity = 0;
	_self->length = 0;
	_self->_payload = NULL;
	_self->_keySize = keySize;
	_self->_valueSize = valueSize;
	return _self;
}


enum Status ordredMap__free(struct OrdredMap* _self)
{
	RETURN_NULL_SELF(_self);
	ordredMap__clear(_self);
	free(_self);
	return OK;
}


enum Status ordredMap__resizeTo(struct OrdredMap* _self, const size_t size)
{
	RETURN_NULL_SELF(_self);
	if(requSize <_self->length)
		return RangeError;
	if(requSize == _self->capacity)
		return OK;

	uint8_t* mem = realloc(_self->_payload, _self->_keySize + sizeof(Container) * requSize);
	if(mem == NULL)
		return MemAllocationError;

	#ifdef DEBUG
	printf(" Vector::Reallocated from %lu capacity to %lu.\n", _self->capacity, requSize);
	#endif

	/* Changing vector members. */
	_self->capacity = requSize;
	_self->_payload = mem;
	return OK;
}


enum Status ordredMap__push(struct OrdredMap* _self, const void* key, const void* value)
{
	RETURN_NULL_SELF(_self);
	if(item == NULL)
		return Error;
	if(_self->length == SIZE_MAX)
		return MemAllocationError;



	

	_self->length++;
	return OK;
}


enum Status ordredMap__removeBy(struct OrdredMap* _self, const void* key);


void* ordredMap__elementBy(struct OrdredMap* _self, const void* key);
