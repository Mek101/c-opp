#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "vector.h"
#include "../itinerator/itinerator.h"

int main()
{
	/*
	struct Vector* vect = vector__alloc(sizeof(int));

	{
		size_t i;
		for(i = 0; i < 10; i++)
		{
			int* val = malloc(sizeof(int));
			*val = i;
			vector__push(vect, val);
		}

		vector__removeRange(vect, 15, vect->length - 10);
		

		vector__resize(vect);

		vector__removeAt(vect, 3);


		int k = 10;
		vector__push(vect, &k);

		vector__contains(vect, &k);

		vector__removeAll(vect, &k);

		vector__removeRange(vect, 9, vect->length);


		struct Itinerator* it = vector__getItinerator(vect);
		uint8_t res;
		it__reset(it);
		do
		{
			printf("%d\n", it__getCurrentAs(it, int));
			res = it__tryMove(it);
		}
		while(res);
		it__destroy(it);

		int arr[16];
		for(i = 0; i < 16; i++)
			arr[i] = 100 + i;

		vector__append(vect, 16, arr);

		vector__pushAll(vect, 4, &arr[0], &arr[1], &arr[2], &arr[3]);

		/* Checking for security... *
		for(i = 0; i < vect->length * vect->_itemSize; i += vect->_itemSize)
		{
			printf("Item at %lu:\n Raw access: %d\n", i, (int)vect->_payload[i]);
			printf(" vector__elementAt access: %d\n", vector__elementAtAs(vect, i / vect->_itemSize, int));
		}
	}

	printf("\n");

	{
		struct Itinerator* it2 = vector__getItinerator(vect);
		/*
		itinerate(it2, tmp, int)
			printf("Itinerating: %d\n", tmp);
		*
		uint8_t __hasNext = 1;
		int c;
		struct Itinerator* __i = it2;
		for(
				it__reset(__i), c = it__getCurrentAs(__i, int);
				__hasNext;
				__hasNext = it__tryMove(__i), c = it__getCurrentAs(__i, int)
			)
			printf("Itinerating: %d\n", c);
		it__destroy(it2);

	}*/

	{
		struct Vector vect = vector__decl(sizeof(int));

		int i = 10;
		vector__push(&vect, &i);
		vector__push(&vect, &i);
		vector__push(&vect, &i);
		vector__push(&vect, &i);
		vector__push(&vect, &i);

		printf("LINE: %d, len: %lu, cap: %lu \n", __LINE__, vect.length, vect.capacity);


		vector__resize(&vect);
		vector__removeRange(&vect, 1, 4);

		printf("LINE: %d, len: %lu, cap: %lu \n", __LINE__, vect.length, vect.capacity);


		vector__resize(&vect);

		printf("LINE: %d, len: %lu, cap: %lu \n", __LINE__, vect.length, vect.capacity);

		vector__clear(&vect);
	}


	{
		struct Vector vect = vector__decl(sizeof(int));

		int i = 10;
		vector__push(&vect, &i);
		vector__push(&vect, &i);
		vector__push(&vect, &i);
		vector__push(&vect, &i);
		vector__push(&vect, &i);

		struct Itinerator* it = vector__getItinerator(&vect);
		it__reset(it);
		do
			printf("%d\n", it__getCurrentAs(it, int));
		while(it__tryMove(it));
		it__destroy(it);

		vector__clear(&vect);
	}

	return 0;
}
