#ifndef VECTOR_H
#define VECTOR_H
#include <stdint.h>
#include <stdlib.h> /* for "size_t" */
#include "../default.h"
#include "../array/array.h"

/*
	Vector struct
*/
struct Vector
{
	/* Number of current elements. */
	size_t length;
	/* The payload size in elements. */
	size_t capacity;

	/* Pointer to the bytes making the memory area. */
	uint8_t* _payload;

	/* Size in bytes of the elements. */
	uint8_t _itemSize;
};


#define vector__decl(itemSize) { 0, 0, NULL, (itemSize) }

/*
	Resets the Vector to it's initial state by freeing the payload.
*/
enum Status vector__clear(struct Vector* _self);


struct Vector* vector__alloc(const uint8_t itemSize);

enum Status vector__free(struct Vector* _self);


/*
	Deep copies the vector.
*/
struct Vector* vector__clone(const struct Vector* _self);


#define vector__resize(v) ( vector__resizeTo((v), (v)->length) )

enum Status vector__resizeTo(struct Vector* _self, const size_t size);


/*
	Pushes a itemSize element into the vector.
	A number of bytes equal to the vectors's 'itemSize' is copied from the item address to the vector's array.
*/
enum Status vector__push(struct Vector* _self, const void* item);

/*
	Pushes a variadic number of itemSize elements into the vector.
	A number of bytes equal to the vectors's 'itemSize' is copied from the item address to the vector's array for each
	element.
*/
enum Status vector__pushAll(struct Vector* _self, const size_t itemCount, ...);

/*
	Pushes an array of itemCount itemSize elements into the vector.
	A number of bytes equal to the vectors's 'itemSize' for 'itemCount' is copied from the item address to the vector's
	array.
*/
enum Status vector__append(struct Vector* _self, const size_t itemCount, const void* items);


/*
	Removes all the itemSize items from vector.
*/
enum Status vector__removeAll(struct Vector* _self, const void* item);

#define vector__removeAt(v, i) ( vector__removeRange((v), (i), (i) + 1) )

enum Status vector__removeRange(struct Vector* _self, const size_t start, const size_t end);


/*
	Checks if the vector contains an itemSize item.
	Returns 1 if it contains it, 0 otherwise.
*/
uint8_t vector__contains(const struct Vector* _self, const void* item);


void* vector__elementAt(const struct Vector* _self, const size_t index);

#define vector__elementAtAs(v, i, t) ( *(t*)vector__elementAt((v), (i)) )


/* Requires both the array container and the itinerator support structure. */
#if defined ITINERATOR_H && defined ARRAY_H
#define vector__getItinerator(v) ( array__getItinerator((v)->_payload, (v)->_itemSize, (v)->length) )
#endif

#endif