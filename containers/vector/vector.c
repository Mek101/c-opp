#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include "vector.h"
#include "../default.h"
#include "../itinerator/itinerator.h"

/*#define DEBUG*/
/*#define RANGE_CHECK*/


/***************************************************************************************************
	#### PRIVATE MEMBERS ####
***************************************************************************************************/
#define __itemPtrAt(ptr, itemSize, i) ( (uint8_t*)((ptr) + (itemSize) * (i)) )

#define __payloadAt(v, i) ( __itemPtrAt(v->_payload, v->_itemSize, i) )


/*
	Gets the next power of 2 to the given value.
*/
static size_t __vector__nextPow(size_t value)
{
	/* https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 Hack. */
	if(value < 2) return 2;

	value--;

	value |= value >> 1;
	value |= value >> 2;
	value |= value >> 4;
	value |= value >> 8;
	value |= value >> 16;
	#ifdef ARCH_64
	value |= value >> 32;
	#endif

	value++;

	/*
		'size_t' is often of a weird size (eg: sometimes is neither 32 or 64 bits: may even be 48
		bits or weirder), so correcting to the maximum value if it has passed it.
	*/
	return value > SIZE_MAX ? SIZE_MAX : value;
}


/*
	Confronts itemSize bytes from the given pointers.
	Returns 1 if the contents where equal, 0 otherwise.
*/
static uint8_t __vector__itemCompare(const size_t itemSize, const void* it1, const void* it2)
{
	size_t i;
	for(i = 0; i < itemSize; i++)
		if(*__itemPtrAt(it1, itemSize, i) != *__itemPtrAt(it2, itemSize, i))
			return 0;
	return 1;
}


/*
	Ensures the vector payload has at least the given capacity for itemSize elements.
	Espects a non-null vector.
*/
static enum Status __vector__ensurePayloadCapacity(struct Vector* _self, const size_t reqCapacity)
{
	if(_self->capacity < reqCapacity)
	{
		#ifdef DEBUG
		printf(" Vector::Not enough capacity %lu for %lu° items.\n", _self->capacity, reqCapacity);
		#endif

		/* Adding capacity by nearest power of 2 for enough space. */
		return vector__resizeTo(_self, __vector__nextPow(reqCapacity));
	}
	else if(_self->capacity / 3 > reqCapacity)
	{
		#ifdef DEBUG
		printf(" Vector::Too much memory wasted %lu for %lu° items.\n", _self->capacity, reqCapacity);
		#endif
		/*
			Too much memory wasted, restricting the vector to the nearest powet of 2 from a third of
			it's size.
		*/
		return vector__resizeTo(_self, __vector__nextPow(reqCapacity / 3));
	}
	else
		return OK;
}


/***************************************************************************************************
	#### PUBLIC MEMBERS ####
***************************************************************************************************/

struct Vector* vector__alloc(const uint8_t itemSize)
{
	if(itemSize == 0)
		return NULL;

	struct Vector*_self = malloc(sizeof(struct Vector));
	if(_self == NULL)
		return NULL;

	_self->length = 0;
	_self->capacity = 0;
	_self->_itemSize = itemSize;
	_self->_payload = NULL;
	return _self;
}


enum Status vector__free(struct Vector* _self)
{
	RETURN_NULL_SELF(_self);
	free(_self->_payload);
	free(_self);
	return OK;
}


/*
	Resets the Vector to it's initial state by freeing the payload. Assumes the payload contents had been cleared by the user.
*/
enum Status vector__clear(struct Vector*_self)
{
	RETURN_NULL_SELF(_self);
	_self->length = 0;
	_self->capacity = 0;
	free(_self->_payload);
	_self->_payload = NULL;
	return OK;
}


struct Vector* vector__clone(const struct Vector* _self)
{
	#ifdef NULL_SELF_CHECK
	if(_self == NULL)
		return NULL;
	#endif
	
	struct Vector* c = malloc(sizeof(struct Vector));
	if(c == NULL)
		return NULL;

	c->_itemSize = _self->_itemSize;
	c->length = _self->length;
	
	/* Clones payload if present. */
	if(_self->length != 0)
	{
		/* Allocates space to the nearest power of 2 to the current length. */
		size_t capacityClone = _self->_itemSize * __vector__nextPow(_self->length - 1);
		void* payloadClone = malloc(capacityClone);
		if(payloadClone == NULL)
			return NULL;

		memcpy(payloadClone, _self->_payload, _self->length * _self->_itemSize);
		c->capacity = capacityClone;
	}
	else
	{
		c->_payload = NULL;
		c->capacity = 0;
	}

	return c;
}


enum Status vector__resizeTo(struct Vector*_self, const size_t requSize)
{
	RETURN_NULL_SELF(_self);
	if(requSize <_self->length)
		return RangeError;
	if(requSize == _self->capacity)
		return OK;

	uint8_t* mem = realloc(_self->_payload, _self->_itemSize * requSize);
	if(mem == NULL)
		return MemAllocationError;

	#ifdef DEBUG
	printf(" Vector::Reallocated from %lu capacity to %lu.\n", _self->capacity, requSize);
	#endif

	/* Changing vector members. */
	_self->capacity = requSize;
	_self->_payload = mem;
	return OK;
}


/*
	Pushes a itemSize element into the vector.
	A number of bytes equal to the vectors's 'itemSize' is copied from the item address to the vector's array.
*/
enum Status vector__push(struct Vector*_self, const void* item)
{
	RETURN_NULL_SELF(_self);
	if(item == NULL)
		return Error;
	if(_self->length == SIZE_MAX)
		return MemAllocationError;

	/* Checks if there's enough space for the new element sitting at the next length. */
	enum Status sts = __vector__ensurePayloadCapacity(_self, _self->length + 1);
	if(sts != OK)
		return sts;

	memcpy(__payloadAt(_self, _self->length), item, _self->_itemSize);
	_self->length++;
	return OK;
}


enum Status vector__pushAll(struct Vector*_self, const size_t itemCount, ...)
{
	RETURN_NULL_SELF(_self);
	if(itemCount == 0)
		return OK;
	/* If the vector has already reached the maximum size or the possible remaining space isn't enough, return error. */
	if(_self->length == SIZE_MAX || SIZE_MAX - _self->length < itemCount)
		return MemAllocationError;

	size_t reqSpace = _self->length + itemCount;
	enum Status sts = __vector__ensurePayloadCapacity(_self, reqSpace);
	if(sts != OK)
		return sts;

	va_list items;
	for(va_start(items, itemCount); _self->length < reqSpace; _self->length++)
		memcpy(__payloadAt(_self, _self->length), va_arg(items, void*), _self->_itemSize);
	va_end(items);

	return OK;
}


/*
	Pushes an array of itemSize elements into the vector.
	a number of bytes equal to the vectors's 'itemSize' is copied from the item address to the vector's array.
*/
enum Status vector__append(struct Vector* _self, const size_t itemCount, const void* items)
{
	RETURN_NULL_SELF(_self);
	if(itemCount == 0)
		return OK;
	/* If the vector has already reached the maximum size or the possible remaining space isn't enough, return error. */
	if(_self->length == SIZE_MAX || SIZE_MAX - _self->length < itemCount)
		return MemAllocationError;

	/* The minimum requested capacity to fit the new items. */
	enum Status sts = __vector__ensurePayloadCapacity(_self, _self->length + itemCount);
	if(sts != OK)
		return sts;

	memcpy(__payloadAt(_self, _self->length), items, _self->_itemSize * itemCount);
	_self->length += itemCount;
	return OK;
}


enum Status vector__removeAll(struct Vector* _self, const void* item)
{
	RETURN_NULL_SELF(_self);
	/* Itinerates all the payload. Doesn't increase the index of it matches since it will be removed. */
	int i;
	for(i = 0; i < _self->length; )
	{
		if(__vector__itemCompare(_self->_itemSize, _self->_payload, item))
		{
			enum Status sts = vector__removeRange(_self, i, i + 1);
			if(sts != OK) return sts;
		}
		else
			i++;
	}
	return OK;
}


enum Status vector__removeRange(struct Vector* _self, const size_t start, const size_t end)
{
	RETURN_NULL_SELF(_self);
	if(start >= end || start >= _self->length || end > _self->length)
		return RangeError;

	/* Reduces the new length. */
	_self->length -= (end - start);

	#ifdef DEBUG
	printf(" Vector::Removing items form %lu to %lu, leaving with %lu items.\n", start, end, _self->length);
	#endif

	/*
		If the items to remove extend up to the length size, it removes all items. Otherwise copies the elements starting
		from the end index, up to the number of elements after the end.
	 */
	if(_self->length == 0)
		return vector__clear(_self);
	else
	{
		/*
			Starts overwriting by moving to the first element from the last one, for a size equal to the difference
			between length and end.
		*/
		memmove(__payloadAt(_self, start), __payloadAt(_self, end), _self->length);

		return __vector__ensurePayloadCapacity(_self, _self->length);
	}
}


uint8_t vector__contains(const struct Vector* _self, const void* item)
{
	#ifdef NULL_SELF_CHECK
	if(_self != NULL && _self->_payload != NULL)
	#else
	if(_self->_payload != NULL)
	#endif
	{
		size_t i;
		for(i = 0 ; i < _self->length; i++)
			if(__vector__itemCompare(_self->_itemSize, _self->_payload, item))
				return 1;
	}
	return 0;
}


void* vector__elementAt(const struct Vector* _self, const size_t index)
{
	#ifdef RANGE_CHECK
		#ifdef NULL_SELF_CHECK
	if(_self == NULL || _self->_payload == NULL || index > _self->length)
		#else
	if(_self->_payload == NULL || index > _self->length)
		#endif
		return NULL;
	else
	#else
		return __payloadAt(_self, index);
	return __payloadAt(_self, index);
	#endif
}
