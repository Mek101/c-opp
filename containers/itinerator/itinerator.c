#include "itinerator.h"
#include <stdlib.h>


struct Itinerator* it__getItinerator(
	void* status,
	void (*r_ptr)(struct Itinerator*),
	uint8_t (*tm_ptr)(struct Itinerator*),
	void* (*gc_ptr)(const struct Itinerator*),
	void (*d_ptr)(struct Itinerator*)
)
{
	struct Itinerator* it = malloc(sizeof(struct Itinerator));
	it->_status = status;
	it->_reset = r_ptr;
	it->_tryMove = tm_ptr;
	it->_getCurrent = gc_ptr;
	it->_destroy = d_ptr;
	return it;
}
