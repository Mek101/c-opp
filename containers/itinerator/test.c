#include "itinerator.h"
#include <stdlib.h>
#include <stdio.h>

#define sts(x) ( (struct Sts*)(x) )

struct Sts
{
	int* arr;
	size_t length;
	size_t i;
};


char fun_tm(void* s)
{
	if((sts(s)->i + 1) < sts(s)->length)
	{
		sts(s)->i++;
		return 1;
	}
	else
		return 0;
}

void* fun_c(void* s)
{
	return &sts(s)->arr[sts(s)->i];
}

void fun_d(void* s)
{
	free(sts(s)->arr);
	free(s);
}


int main()
{
	struct Sts* s = malloc(sizeof(struct Sts));
	s->arr = calloc(10, sizeof(int));
	s->length = 10;
	s->i = 0;
	size_t k;
	for(k = 0; k < 10; k++)
		s->arr[k] = k*2;


	struct Itinerator* it = getItinerator(s, fun_tm, fun_c, fun_d);

	while(tryMove(it))
		printf("Moved to val %d.\n", it__getCurrentAs(it, int));
}
