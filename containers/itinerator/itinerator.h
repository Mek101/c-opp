#include <stdint.h>

#ifndef ITINERATOR_H
#define ITINERATOR_H

#include "../default.h"
#include "../itinerator/itinerator.h"


struct Itinerator
{
	void* _status;
	void (*_reset)(struct Itinerator*);
	uint8_t (*_tryMove)(struct Itinerator*);
	void* (*_getCurrent)(const struct Itinerator*);
	void (*_destroy)(struct Itinerator*);
};


struct Itinerator* it__getItinerator(
	void* status,
	void (*_reset)(struct Itinerator*),
	uint8_t (*_tryMove)(struct Itinerator*),
	void* (*_getCurrent)(const struct Itinerator*),
	void (*_destroy)(struct Itinerator*)
);


#define it__reset(i) (	\
	(i)->_reset(i)		\
)

#define it__tryMove(i) (	\
	(i)->_tryMove(i)		\
)

#define it__getCurrent(i) (	\
	(i)->_getCurrent(i)		\
)

#define it__getCurrentAs(i, t) (	\
	*(t*)(it__getCurrent(i))		\
)

#define it__destroy(i) (	\
	(i)->_destroy(i)		\
)


#define __varConcatDefHelper(x, y) x##y
#define __vcImpl(x, y) __vcDefHelper(x, y)
#define __vc(x) __vcImpl(x, __LINE__)

/* Bellow: all in one line so the internal variables are all named after the line. */
#define itinerate(it, c, type) \
	type c; \
	struct Itinerator* __vc(i) = (it); uint8_t __vc(hn) = 1; for(it__reset(__i), c = it__getCurrentAs(vc(i), type);__vc(hn); __hasNext = it__tryMove(__vc(i)), c = it__getCurrentAs(__vc(i), type))



#endif
