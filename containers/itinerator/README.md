## How to itinerate:

During it's lifetime, the itinerator assumes the collection it's itinerating will be always be valid and it's members won't change, therefore do NOT alter the collection or free it during the itinerator lifetime (unless you're going to dispose of it already or you’re ok with invalidating it).

The itinerator starts of in an undefined state, therefore it's necessary to initialize it with the
```c
void it__reset(struct Itinerator* _self)
```
parametric macro, which can also be used to reset a consumed itinerator to it's initial, valid state.

The itinerator always starts from the first element of the collection, therefore the
```c
void* it__getCurrent(struct Itinerator* _self)
type it__getCurrentAs(struct Itinerator* _self, type)
```
parametric macros will start by returning the first element.


To itinerate to the next element, call the 
```c
char it__tryMove(struct Itinerator* _self)
```
parametric macro, which will try to itinerate to the next collection’s element. It will return a ‘char’ value
determining if the itineration has been successful or not. If the result is 0, then it was unsuccessful and therefore
the itinerator has finished itinerating the collection and it’s considered indeterminate behavior calling either
‘it\_\_getCurrent’ or ‘it\_\_getCurrentAs’ on it, since it may vary on the underlying collection. Otherwise if the result is different than 0, it
is safe to use either ‘it\_\_getCurrent’ or ‘it\_\_getCurrentAs’ to access the new element.


Once an itinerator has finished it’s job, dispose of it by calling the 
```c
void it__destroy(struct Itinerator* _self)
```
parametric macro, which will proceed to free the itinerator resources, invalidating it.


The itinerator guarantees that it will not modify the underlying collection in any way, and it’s safe to use and/or
alter the collection once the itinerator has been disposed of.

It’s recommended to use the itinerator in a ‘do-while’ loop. Example:
```c
struct Itinerator* it = collection__getItinerator(col);
do
{
    char* c0 = it__getCurrent(it);
	char c1 = it__getCurrentAs(it, char);
}
while(it__tryMove(it));
it__destroy(it);
```
