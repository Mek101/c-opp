#include <stdio.h>
#include "queue.h"


int main()
{
	struct Queue* queue = queue__alloc(sizeof(int));
	int arr[10];
	size_t i;
	for(i = 0; i < 10; i++)
	{
		printf("Loading %luish number.\n", i);
		arr[i] = i*i;
		queue__enqueue(queue, &arr[i]);
	}

	printf("\n");

	struct Itinerator* it = queue__getItinerator(queue);
	it__reset(it);
	do
		printf("%d.\n", it__getCurrentAs(it, int));
	while(it__tryMove(it));
	it__destroy(it);

	printf("\n");

	enum Status res;
	do
	{
		int c;
		res = queue__dequeue(queue, &c);
		printf("lentgh: %lu, res: %d, val: %d\n", queue->length, res, c);
	}
	while(queue->length != 0 && res == OK);

	queue__free(queue);

	return 0;
}
