#ifndef QUEUE_H
#define QUEUE_H

#include "../default.h"
#include "../list/list.h"
#include "../list/container.h"


#define Queue List

#define queue__decl list__decl

#define queue__clear list__clear


#define queue__alloc list__alloc

#define queue__free list__free


enum Status queue__enqueue(struct Queue* _self, void* item);

enum Status queue__dequeue(struct Queue* _self, void* item);


#define queue__getFront list__getFront

#define queue__getTail list__getTail


#define queue__getItinerator list__getItinerator


#endif
