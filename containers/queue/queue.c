#include <stdio.h>
#include "queue.h"
#include "../list/container.h"
#include "../default.h"

/*#define DEBUG*/

/***************************************************************************************************
	#### PUBLIC MEMBERS ####
***************************************************************************************************/

enum Status queue__enqueue(struct Queue* _self, void* item)
{
	RETURN_NULL_SELF(_self);

	Container* c = container__alloc(NULL, item, _self->itemSize);
	if(c == NULL)
		return MemAllocationError;

	/*
		If there is a tail container, push the new item and update it. Set the front item otherwise.
	 */
	Container* tail = _list__getTailContainer(_self->_front);

	#ifdef DEBUG
	printf(" Queue::Got tail: %p\n", tail);
	#endif

	if(tail == NULL)
		_self->_front = c;
	else
		container__setNext(tail, c);

	#ifdef DEBUG
	printf(" Queue::front %p next %p\n", _self->_front, container__getNext(_self->_front));
	#endif

	_self->length++;
	
	return OK;
}


enum Status queue__dequeue(struct Queue* _self, void* item)
{
	RETURN_NULL_SELF(_self);
	if(_self->_front == NULL)
		return NullInstance;

	#ifdef DEBUG
	printf(" Queue::front %p, next %p\n", _self->_front, container__getNext(_self->_front));
	#endif

	/* Getting the current front. */
	Container* cnt = _self->_front;
	/* Setting the new front. */;
	_self->_front = container__getNext(cnt);
	
	memcpy(item, container__item(cnt), _self->itemSize);

	container__free(cnt);

	_self->length--;

	return OK;
}
