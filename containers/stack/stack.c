#include <stdio.h>
#include "stack.h"
#include "../default.h"
#include "../list/container.h"

/*#define DEBUG*/

/*
	#### PUBLIC MEMBERS ####
*/

enum Status stack__push(struct Stack* _self, const void* item)
{
	RETURN_NULL_SELF(_self);

	#ifdef DEBUG
	printf(" Stack::Pushing at front: %p.\n", _self->_front);
	#endif

	Container* cnt = container__alloc(_self->_front, item, _self->itemSize);
	if(cnt == NULL)
		return MemAllocationError;

	_self->_front = cnt;
	_self->length++;

	#ifdef DEBUG
	printf(" Stack::New front: %p and next %p\n", _self->_front, container__getNext(_self->_front));
	#endif

	return OK;
}


enum Status stack__pop(struct Stack* _self, void* item)
{
	RETURN_NULL_SELF(_self);
	if(_self->_front == NULL)
		return NullInstance;

	Container* cnt = _self->_front;

	#ifdef DEBUG
	printf(" Stack::Front %p, next: %p. Equal? %s\n", cnt, container__getNext(cnt), cnt == container__getNext(cnt) ? "Yes" : "No");
	#endif

	if(memcpy(item, container__item(cnt), _self->itemSize) == NULL)
		return Error;

	_self->_front = container__getNext(cnt);

	#ifdef DEBUG
	printf(" Stack::Set next, new front: %p\n", _self->_front);
	#endif

	container__free(cnt);
	
	_self->length--;

	#ifdef DEBUG
	printf(" Stack::Freed the container.\n");
	#endif

	return OK;
}
