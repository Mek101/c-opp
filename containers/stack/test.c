#include <stdio.h>
#include "stack.h"


int main()
{
	size_t i;

	{
		int arr[10];
		struct Stack* stk = stack__alloc(sizeof(int));

		for(i = 0; i < 10; i++)
		{
			arr[i] = i;
			stack__push(stk, &arr[i]);
		}

		printf("length: %lu\n", stk->length);

		struct Itinerator* it = stack__getItinerator(stk);
		itinerate(it, tmp, int)
			printf("%d as item.\n", tmp);
		it__destroy(it);

		int ptr;
		do
			stack__pop(stk, &ptr);
		while(stk->length != 0);

		stack__free(stk);
	}

	printf("\n\n");

	{
		double arr2[10];
		struct Stack stk2 = stack__decl(sizeof(double));

		for(i = 0; i < 10; i++)
		{
			arr2[i] = i;
			stack__push(&stk2, &arr2[i]);
		}

		printf("\n");

		double d;
		do
		{
			stack__pop(&stk2, &d);
			printf("%.0f\n", d);
		}
		while(stk2.length != 0);

		stack__clear(&stk2);
	}
}