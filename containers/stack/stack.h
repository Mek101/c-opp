#ifndef STACK_H
#define STACK_H

#include "../default.h"
#include "../list/list.h"
#include "../list/container.h"


#define Stack List


#define stack__decl list__decl

#define stack__clear list__clear


#define stack__alloc list__alloc

#define stack__free list__free


enum Status stack__push(struct Stack* _self, const void* item);

enum Status stack__pop(struct List* _self, void* item);


#define stack__getFront list__getFront

#define stack__getTail list__getTail


#define stack__getItinerator list__getItinerator


#endif
