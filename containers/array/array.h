#ifndef ARRAY_H
#define ARRAY_H

#include <stdlib.h>
#include "../itinerator/itinerator.h"

/* Requires the itinerator. */
#ifdef ITINERATOR_H
struct Itinerator* array__getItinerator(void* _self, const unsigned char itemSize, const size_t length);
#endif

#endif
