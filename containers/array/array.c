#include <stdlib.h>
#include <stdio.h>
#include "array.h"
#include "../itinerator/itinerator.h"

/*#define DEBUG*/
#define sts(x) ( (struct Sts*)((x)->_status) )

/***************************************************************************************************
	#### PRIVATE MEMBERS ####
***************************************************************************************************/

struct Sts
{
	/* Pointer to the initial element of an array of bytes. */
	uint8_t* start;
	/* Pointer at the current element of an array of bytes. */
	uint8_t* current;
	/* Lentgh of the array in bytes. */
	uint8_t* end;
	/* Size of the elements in bytes. */
	uint8_t itemSize;
};


static void __array__reset(struct Itinerator* it)
{
	sts(it)->current = sts(it)->start;
}


static uint8_t __array__tryMove(struct Itinerator* it)
{
	#ifdef DEBUG
	printf(" Array::Pointer: %p, end: %p: moving index to: %lu,\n", sts(it)->current, sts(it)->end, ((sts(it)->current - sts(it)->end) / sts(it)->itemSize));
	#endif
	sts(it)->current += sts(it)->itemSize;
	return sts(it)->current < sts(it)->end;
}


static void* __array__getCurrent(const struct Itinerator* it)
{
	return sts(it)->current;
}


static void __array__destroy(struct Itinerator* it)
{
	#ifdef DEBUG
	printf(" Array::Freeing status.\n");
	#endif
	free(it->_status);

	#ifdef DEBUG
	printf(" Array::Freeing Itinerator.\n");
	#endif
	free(it);
}

/***************************************************************************************************
	#### PUBLIC MEMBERS ####
***************************************************************************************************/

struct Itinerator* array__getItinerator(void* _self, const uint8_t itemSize, const size_t length)
{
	#ifdef NULL_SELF_CHECK
	if(_self == NULL)
		return NULL;
	#endif

	struct Sts* s = malloc(sizeof(struct Sts));
	s->start = _self;
	s->itemSize = itemSize;
	s->end = ((uint8_t*)_self) + length * itemSize;

	return it__getItinerator(s, __array__reset, __array__tryMove, __array__getCurrent, __array__destroy);
}
