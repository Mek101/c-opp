#include <stdio.h>
#include "array.h"
#include "../itinerator/itinerator.h"

#define LENGTH 30

int main()
{
	int* arr = malloc(sizeof(int) * LENGTH);

	int i;
	for(i = 0; i < LENGTH; i++)
		arr[i] = i;

	struct Itinerator* it = array__getItinerator(arr, sizeof(int), LENGTH);
	it__reset(it);
	printf("Item 0 at: %p.\nItem 1 at: %p.\nArray bytesize: %lu.\n", &arr[0], &arr[1], sizeof(int) * LENGTH);

	do
		printf("%d\n", it__getCurrentAs(it, int));
	while(it__tryMove(it));
	it__destroy(it);

	free(arr);

	return 0;
}