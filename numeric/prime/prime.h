#ifndef PRIME_H
#define PRIME_H
#include <stdlib.h>
#include "../../containers/vector/vector.h"
#include "../../containers/itinerator/itinerator.h"


/*
	Gets the Nth prime number or 0 if overflows. The given index must be between 0 and SIZE_MAX - 1.
*/
double prime__getNth(const size_t n);


/*
	Checks if the given number is a prime number. Returns 1 if it is, 0 otherwise or it overflowed.
*/
uint8_t prime__isPrime(const double n);


/*
	Generates and caches a number of prime numbers until the cache reaches the given size, starting from the last cached.
	The given index must be between 0 and SIZE_MAX.	Returns 0 in case of overflows, 1 otherwise.
*/
uint8_t prime__genCacheUntilSize(const size_t end);


/*
	Returns the current size of the cache.
*/
size_t prime__cacheSize(void);


/*
	Destroys the cached prime numbers.
*/
void prime__clearCache(void);


/*
	Compacts the cache memory.
*/
void prime__compactCache(void);


#ifdef ITINERATOR_H
/*
	Generates a semi-infinite number of prime numbers without generating cache. The itinerator terminates on overflows.
*/
struct Itinerator* prime__getPrimeGenerator();
#endif

#endif
