#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <stdint.h> /* for SIZE_MAX */
#include <stdio.h>
#include "prime.h"
#include "../../containers/vector/vector.h"

/*#define DEBUG*/

/* Gets the last prime number in cache. Returns 0 if the cache is empty. */
#define __prime__getLast(v) ( __prime__cache.length == 0 ? 0 : vector__elementAtAs(&v, v.length - 1, double) )

/* Casts a void* to a status struct. */
#define __prime__asCurrent(s) ( (struct Sts*)s->_status )

/* Returns half the number plus it's rest and 1. */
#define __prime__largestHalf(n) ( ((n) / 2) + (fmod((n), 2) + 1) )


/***************************************************************************************************
	#### PRIVATE MEMBERS ####
***************************************************************************************************/
/*
	Vector caching all the prime numbers extracted.
*/
static struct Vector __prime__cache = vector__decl(sizeof(double));


/*
	Gets the first prime number after the given offset. May generate cache. In case of overflow,
	returns 0.
*/
static double __prime__getPrimeAfter(const double offset)
{
	/* Checks for obvious. */
	if(offset < 2)
		return 2;

	double lastCached = __prime__getLast(__prime__cache);

	/* Searches cache. */
	if(offset < lastCached)
	{
		/*
			From last to first: takes a candidate as the next prime, if it's less or equal than the
			offset, then we just passed our number, so the last one was the correct one. If it's
			greater, then it becomes the lastPrime and another one is extracted.
		*/
		size_t i;
		for(i = __prime__cache.length; i > 0; i--)
		{
			double candidate = vector__elementAtAs(&__prime__cache, i - 1, double);

			#ifdef DEBUG
			printf("At index: %lu, lastCached: %.1lf, candidate: %.1lf, offset: %.1lf\n", i - 1, lastCached, candidate, offset);
			#endif

			if(candidate <= offset)
				return lastCached;
			else
				lastCached = candidate;

			#ifdef DEBUG
			printf("lastCached: %.1lf, candidate: %.1lf\n", lastCached, candidate);
			#endif
		}
	}
	/* Generates a new prime number. */
	else
	{
		/*
			Function implementation:
			Makes a candidate prime number major than the offset by testing odd numbers (since the
			only prime pair number is 2), then tries to test it incrementing it by 2 at each faliure.
			If any of the numbers is a prime one, then exits both loops and returns it. If it
			reaches the maximum value of the data type and it hasn't found a prime number in it,
			then it's impossible to continue since an overflow may occur: so it returns 0, a number
			ouside the prime enseble by definition.
		*/

		double candidate = 0;
		/* If the offset is odd, make the candidate pair to add 1 later on. */
		if(fmod(offset, 2) != 0 && offset > 0)
			candidate++;

		for(candidate += offset + 1; candidate < DBL_MAX; candidate += 2)
			if(prime__isPrime(candidate))
				return candidate;

		/*
			If it terminated because it reached the maximum value without finding a prime, then it
			overflew: so returning 0 as error code.
		*/
		return 0;
	}
}


/*
	Generates and caches prime numbers until it the generated prime reaches or surpasses the given end, starting from
	the last cached. Returns 0 in case of overflows, 1 otherwise.
*//*
static char __prime__generateUntilValue(double end)
{
	double lastPrime = __prime__getLast(__prime__cache);
	while(lastPrime < end)
	{
		lastPrime = __prime__getPrimeAfter(lastPrime);
		/* checks for overflows. *
		if(lastPrime == 0)
			return 0;
		vector__push(__prime__cache, &lastPrime);
	}
	return 1;
}
*/


#ifdef ITINERATOR_H
struct Sts
{
	double begin;
	double current;
};


static void __prime__reset(struct Itinerator* it)
{
	__prime__asCurrent(it)->current = __prime__asCurrent(it)->begin;
}


/*
	Generates a new prime number.
*/
static char __prime__tryMove(struct Itinerator* it)
{
	return __prime__asCurrent(it)->current = __prime__getPrimeAfter(__prime__asCurrent(it)->current);
}


/*
	Returns the current prime number.
*/
static void* __prime__getCurrent(struct Itinerator* it)
{
	return &__prime__asCurrent(it)->current;
}
#endif


/***************************************************************************************************
	#### PUBLIC MEMBERS ####
***************************************************************************************************/
/*
	Gets nth prime number. Uses and generates cache. Retuns 0 in case of overflows.
*/
double prime__getNth(const size_t n)
{
	/* Checks for overflows. */
	if(n == SIZE_MAX || prime__genCacheUntilSize(n + 1) == 0)
	{
		/* Compacts the fullied cache. */
		vector__resize(&__prime__cache);
		return 0;
	}
	else
		return vector__elementAtAs(&__prime__cache, n, double);
}


/*
	Establishes if the given number is a prime number. Will firstly test the number using the cached
	prime numbers, but won't generate cache.
*/
uint8_t prime__isPrime(const double n)
{
	if(n < 2)
		return 0;

	/* If it's not a known prime or divisible by any prime number, then it isn't a prime number. */
	size_t i;
	for(i = 0; i < __prime__cache.length; i++)
	{
		double candidateDiv = vector__elementAtAs(&__prime__cache, i, double);
		if(n == candidateDiv)
			return 1;
		if(candidateDiv > n || fmod(n, candidateDiv) == 0)
			return 0;
	}

	/*
		If the cache was smaller than the largest half of the number, there may be divisors futher
		ahead, so continue testing with odd numbers, starting from the next odd from the last prime
		cached.
	*/
	const double largestHalf = __prime__largestHalf(n);
	double candidateDiv = __prime__getLast(__prime__cache);
	/* Testing with the first and only pair only number if there are none cached. */
	if(candidateDiv == 0)
	{
		if(n == 2)
			return 1;
		if(fmod(n, 2) == 0)
			return 0;
		candidateDiv = 1;
	}

	while(candidateDiv < largestHalf)
	{
		/* Jumping 2 in 2, skipping pair numbers. */
		candidateDiv += 2;
	
		#ifdef DEBUG
		printf("Attempting division by: %lf\n", candidateDiv);
		#endif
		if(fmod(n, candidateDiv) == 0 && n != candidateDiv)
			return 0;
	}

	return 1;
}


/*
	Generates and caches a number of prime numbers until the cache reaches the given size, starting
	from the last cached. Returns 0 in case of overflows, 1 otherwise.
*/
uint8_t prime__genCacheUntilSize(const size_t end)
{
	double lastPrime = __prime__getLast(__prime__cache);

	/* Keep pushing until the desired size is reached. */
	while(__prime__cache.length < end)
	{
		lastPrime = __prime__getPrimeAfter(lastPrime);
		/* Checks for overflows. */
		if(lastPrime == 0)
			return 0;

		vector__push(&__prime__cache, &lastPrime);
	}
	return 1;
}


size_t prime__cacheSize(void)
{
	return __prime__cache.length;
}


void prime__clearCache(void)
{
	vector__clear(&__prime__cache);
}


void prime__compactCache(void)
{
	vector__resize(&__prime__cache);
}


#ifdef ITINERATOR_H
struct Itinerator* prime__getPrimeGenerator(const double start)
{
	struct Sts* sts = malloc(sizeof(struct Sts));
	
	/* Setting start at the first prime number. */
	if(prime__isPrime(start))
		sts->begin = start;
	else
		sts->begin = __prime__getPrimeAfter(start);

 	return it__getItinerator(sts, __prime__reset, __prime__tryMove, __prime__getCurrent, (void (*)(struct Itinerator*))free);
}
#endif


#ifdef DEBUG
void prime__viewCache(void)
{
	size_t i;
	for(i = 0; i < __prime__cache->length; i++)
		printf(" prime__viewCache::Cache at %lu: %.1lf\n", i, vector__elementAtAs(__prime__cache, i, double));
}
#endif

