#include <stdio.h>
#include "prime.h"
#include "../../containers/itinerator/itinerator.h"


int main()
{
	size_t i;

	prime__compactCache();
	printf("Last generated at: %.1lf\n", prime__getNth(prime__cacheSize() - 1));

	printf("\n");

	printf("Cache size: %lu\n", prime__cacheSize());
	
	for(i = 0; i < 100; i++)
		if(prime__isPrime(i))
			printf("New prime: %lu\n", i);

	printf("\n");

	for(i = 0; i < prime__cacheSize(); i++)
	{
		double res = prime__getNth(i);
		if(res == 0)
			printf("Overflow with %lu\n", i);
		else
			printf("%lu prime number: %-1.1lf\n", i, res);
	}

	printf("\n");


	struct Itinerator* it = prime__getPrimeGenerator(0);
	i = 0;
	do
	{
		printf("%luish generated prime number: %.1lf\n", i, it__getCurrentAs(it, double));
		i++;
	}
	while(it__tryMove(it) && i < 30);
	it__destroy(it);

	return 0;
}
